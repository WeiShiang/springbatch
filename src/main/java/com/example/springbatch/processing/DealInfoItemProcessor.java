package com.example.springbatch.processing;

import com.example.springbatch.vo.DealInfo;
import org.springframework.batch.item.ItemProcessor;

public class DealInfoItemProcessor implements ItemProcessor<DealInfo, DealInfo> {

    public DealInfo process(final DealInfo dealInfo) throws Exception {
        final String securityCode = dealInfo.getSecurityCode();
        final String securityName = dealInfo.getSecurityName();
        final String tradeVolume = dealInfo.getTradeVolume();
        final String tradeValue = dealInfo.getTradeValue();
        final String openingPrice = dealInfo.getOpeningPrice();
        final String highestPrice = dealInfo.getHighestPrice();
        final String lowestPrice = dealInfo.getLowestPrice();
        final String closePrice = dealInfo.getClosePrice();
        System.out.println(dealInfo.toString());
        return new  DealInfo(securityCode, securityName, tradeVolume, tradeValue,  openingPrice,
                             highestPrice, lowestPrice, closePrice);
    }
}
