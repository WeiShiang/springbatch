package com.example.springbatch.config;

import com.example.springbatch.listener.JobCompletionNotificationListener;
import com.example.springbatch.processing.DealInfoItemProcessor;
import com.example.springbatch.vo.DealInfo;
import com.google.gson.Gson;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.json.GsonJsonObjectReader;
import org.springframework.batch.item.json.JsonItemReader;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public JsonItemReader<DealInfo> jsonItemReader() throws Exception {

        // configure the objectMapper as required
        GsonJsonObjectReader<DealInfo> jsonObjectReader =  new GsonJsonObjectReader<>(DealInfo.class);
        jsonObjectReader.setMapper(new Gson());
        jsonObjectReader.open(new ClassPathResource("deal-info.json"));
        jsonObjectReader.read();
        jsonObjectReader.close();


        return new JsonItemReaderBuilder<DealInfo>()
                .jsonObjectReader(jsonObjectReader)
                .resource(new ClassPathResource("deal-info.json"))
                .name("dealInfoJsonItemReader")
                .build();
    }

//    @Bean
//    public FlatFileItemReader<DealInfo> reader() {
//        return new FlatFileItemReaderBuilder<DealInfo>()
//                .name("personItemReader")
//                .resource(new ClassPathResource("deal-info.json"))
//                .delimited()
//                .names(new String[]{"securityCode", "securityName", "tradeVolume", "tradeValue", "openingPrice",
//                        "highestPrice", "lowestPrice", "closePrice", "change", "transaction"})
//                .fieldSetMapper(new BeanWrapperFieldSetMapper<DealInfo>() {{
//                    setTargetType(DealInfo.class);
//                }})
//                .build();
//    }

    @Bean
    public DealInfoItemProcessor processor() {
        return new DealInfoItemProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<DealInfo> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<DealInfo>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO db2odm.dealinfo (security_code, security_name, trade_volume, trade_value, opening_price," +
                        " highest_price, lowest_price, close_price) " +
                        "VALUES (:securityCode, :securityName, :tradeVolume, :tradeValue, :openingPrice," +
                        ":highestPrice, :lowestPrice, :closePrice)")
                .dataSource(dataSource)
                .build();
    }

    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<DealInfo> writer) throws Exception {
        return stepBuilderFactory.get("step1")
                .<DealInfo, DealInfo>chunk(10)
                .reader(jsonItemReader())
                .processor(processor())
                .writer(writer)
                .build();
    }
}
