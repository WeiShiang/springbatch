package com.example.springbatch.vo;

public class DealInfo {
    String securityCode;
    String securityName;
    String tradeVolume;
    String tradeValue;
    String openingPrice;
    String highestPrice;
    String lowestPrice;
    String closePrice;

    public DealInfo() {
    }

    public DealInfo(String securityCode, String securityName, String tradeVolume, String tradeValue, String openingPrice, String highestPrice, String lowestPrice, String closePrice) {
        this.securityCode = securityCode;
        this.securityName = securityName;
        this.tradeVolume = tradeVolume;
        this.tradeValue = tradeValue;
        this.openingPrice = openingPrice;
        this.highestPrice = highestPrice;
        this.lowestPrice = lowestPrice;
        this.closePrice = closePrice;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getSecurityName() {
        return securityName;
    }

    public void setSecurityName(String securityName) {
        this.securityName = securityName;
    }

    public String getTradeVolume() {
        return tradeVolume;
    }

    public void setTradeVolume(String tradeVolume) {
        this.tradeVolume = tradeVolume;
    }

    public String getTradeValue() {
        return tradeValue;
    }

    public void setTradeValue(String tradeValue) {
        this.tradeValue = tradeValue;
    }

    public String getOpeningPrice() {
        return openingPrice;
    }

    public void setOpeningPrice(String openingPrice) {
        this.openingPrice = openingPrice;
    }

    public String getHighestPrice() {
        return highestPrice;
    }

    public void setHighestPrice(String highestPrice) {
        this.highestPrice = highestPrice;
    }

    public String getLowestPrice() {
        return lowestPrice;
    }

    public void setLowestPrice(String lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    public String getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(String closePrice) {
        this.closePrice = closePrice;
    }

    @Override
    public String toString() {
        return "DealInfo{" +
                "securityCode='" + securityCode + '\'' +
                ", securityName='" + securityName + '\'' +
                ", tradeVolume='" + tradeVolume + '\'' +
                ", tradeValue='" + tradeValue + '\'' +
                ", openingPrice='" + openingPrice + '\'' +
                ", highestPrice='" + highestPrice + '\'' +
                ", lowestPrice='" + lowestPrice + '\'' +
                ", closePrice='" + closePrice + '\'' +
                '}';
    }
}
